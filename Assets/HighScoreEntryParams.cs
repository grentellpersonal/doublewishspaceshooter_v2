﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreEntryParams : MonoBehaviour {
	public Text username;
	public Text score;
}
