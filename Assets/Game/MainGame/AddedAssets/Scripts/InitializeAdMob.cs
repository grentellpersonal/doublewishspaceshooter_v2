﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class InitializeAdMob : MonoBehaviour {

	private BannerView bannerView;

	void Start () {
		#if UNITY_ANDROID
		string appId = "ca-app-pub-2077096036883130~1611104032";
		#elif UNITY_IPHONE
		string appId = "unexpected_platform";
		#else
		string appId = "unexpected_platform";
		#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(appId);
	}

	void Update () {
		
	}
}
