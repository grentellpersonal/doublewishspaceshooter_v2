﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed;
	public Boundary boundary;
	public float tilt;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate = 0.5f;

	private float nextFire = 0.0f;

	private Rigidbody rb;
	private Transform oldPosition;
	private AudioSource audioSource;

	void Start() {
		rb = GetComponent<Rigidbody> ();
		audioSource = GetComponent<AudioSource> ();
	}

	void Update() {
		if (Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
			audioSource.Play ();
		}
	}

	void FixedUpdate() {
		
		if (Input.GetMouseButtonDown (0) || Input.GetMouseButton(0)) {
			Vector3 mousePosition = Input.mousePosition;
			mousePosition = Camera.main.ScreenToWorldPoint (mousePosition);

			mousePosition = new Vector3 (
				Mathf.Clamp(mousePosition.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp(mousePosition.z, boundary.zMin, boundary.zMax)
			);

			float velocity = mousePosition.x - transform.position.x;
			transform.rotation = Quaternion.Euler (0.0f, 180.0f, velocity * tilt);

			transform.position = Vector3.Lerp (transform.position, mousePosition, speed);
		}
	}

}
