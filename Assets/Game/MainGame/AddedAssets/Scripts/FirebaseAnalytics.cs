﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseAnalytics : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("Began game", "Text", 0.1f);
		Firebase.Analytics.FirebaseAnalytics.LogEvent (Firebase.Analytics.FirebaseAnalytics.EventLogin);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
