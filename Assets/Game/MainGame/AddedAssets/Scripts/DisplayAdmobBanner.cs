﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class DisplayAdmobBanner : MonoBehaviour {

	private BannerView bannerView;

	// Use this for initialization
	void Start () {
		this.RequestBanner();
	}
	
	private void RequestBanner() {
		
		string adUnitId = "ca-app-pub-2077096036883130/2172720126";


		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();

		// Load the banner with the request.
		bannerView.LoadAd(request);
	}

}
