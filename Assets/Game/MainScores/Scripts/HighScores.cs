﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

using UnityEngine;
using UnityEngine.SocialPlatforms;

public class HighScores : MonoBehaviour {

	public HighScoreEntryParams[] highScoreEntry;

	public void Start() {

		for (int i = 0; i < highScoreEntry.Length; i++) {
			highScoreEntry [i].username.text = "";
			highScoreEntry [i].score.text = "";
		}

		PlayGamesPlatform.Instance.LoadScores (GPGSIds.leaderboard_number_of_objects_hit,
			LeaderboardStart.TopScores,
			highScoreEntry.Length,
			LeaderboardCollection.Public,
			LeaderboardTimeSpan.AllTime,
			(data) => {
				Debug.Log("Leaderboard data valid: " + data.Valid);
				Debug.Log("approx: " + data.ApproximateCount + " have " + data.Scores.Length);

				List<string> userIds = new List<string>();

				foreach(IScore score in data.Scores) {
					userIds.Add(score.userID);
				}

				Social.LoadUsers(userIds.ToArray(), (users) => {
					for(int i=0; i < data.Scores.Length; i++) {
						IUserProfile user = FindUser(users, data.Scores[i].userID);

						string username = user.userName;
						if(username.Length > 15) {
							username = username.Substring(0, 14) + "...";
						}

						highScoreEntry[i].username.text = username;
						highScoreEntry[i].score.text = data.Scores[i].value.ToString();
					}
				});

			});

	}

	private IUserProfile FindUser(IUserProfile[] users, string userid) {
		foreach (IUserProfile user in users) {
			if (user.id == userid) {
				return user;
			}
		}
		return null;
	}

}
