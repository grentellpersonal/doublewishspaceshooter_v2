﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

public class ViewLeaderboards : MonoBehaviour {
	public void ShowLeaderBoards() {
		if (PlayGamesPlatform.Instance.localUser.authenticated) {
			PlayGamesPlatform.Instance.ShowLeaderboardUI ();
		} else {
			Debug.Log ("Cannot show leaderboard: not authenticated");
		}
	}
}
