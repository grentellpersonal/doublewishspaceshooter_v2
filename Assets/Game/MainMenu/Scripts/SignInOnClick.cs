﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.UI;

public class SignInOnClick : MonoBehaviour {

	public Animator mainMenuAnimator;
	public Animator startScreenAnimator;
	public Text authorizationStatus;
	public Text signInFailedStatus;

	private bool signinClicked = false;

	public void Start() {
		signInFailedStatus.text = "";
		// Create client configuration
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder ().Build ();

		// Enable debugging output
		PlayGamesPlatform.DebugLogEnabled = true;

		// Initialize and activate the platform
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.Activate ();

		PlayGamesPlatform.Instance.Authenticate (SignInCallback, true);
	}

	public void SignIn() {
		Debug.Log ("signInButton clicked!");
		signinClicked = true;
		if (!PlayGamesPlatform.Instance.localUser.authenticated) {
			PlayGamesPlatform.Instance.Authenticate (SignInCallback, false);
		}
	}

	public void SignOut() {
		// Sign out of play games
		PlayGamesPlatform.Instance.SignOut ();

		// Reset UI
		authorizationStatus.text = "";
		signInFailedStatus.text = "";
		mainMenuAnimator.SetTrigger ("Close");
		startScreenAnimator.SetTrigger ("Open");
		signinClicked = false;
	}

	public void SignInCallback(bool success) {
		if (success) {
			Debug.Log ("User Signed In!");
			signInFailedStatus.text = "";
			startScreenAnimator.SetTrigger ("Close");
			mainMenuAnimator.SetTrigger ("Open");
			authorizationStatus.text = "Signed in as: " + Social.localUser.userName;
			signinClicked = false;
		} else {
			Debug.Log ("Sign in Failed!");

			if (signinClicked)
				StartCoroutine (SetSignInFailed("Sign In Failed..."));
			
			startScreenAnimator.SetTrigger ("Open");
		}
	}

	public IEnumerator SetSignInFailed(string text) {
		yield return new WaitForSeconds (1.0f);
		signInFailedStatus.text = text;
	}
}
